
var BaseURL='http://www.oschina.net/action/api/'
module.exports={
  news_list:BaseURL+'news_list',
  news_detail:BaseURL+'news_detail',

  login:BaseURL+'login_validate',
  my_information:BaseURL+'my_information',
  favorite_list:BaseURL+'favorite_list',
  find_user:BaseURL+'find_user',

  blog_list:BaseURL+'blog_list',
  blog_detail:BaseURL+'blog_detail',

  software_detail:BaseURL+'software_detail',

  tweet_list:BaseURL+'tweet_list',

  rock_rock:BaseURL+'rock_rock',
}
