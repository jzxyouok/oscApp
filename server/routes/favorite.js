var express = require('express');
var httpHelper = require('../utils/httpHelper')
var xml2js = require('xml2js');
var parseString = xml2js.parseString;

var OSCAPI = require('../utils/OSCAPI')

var router = express.Router();

/**
* @description 根据type查询我的收藏
* @param uid（required） 用户的uid
* @param type（required）
*  0 全部收藏　
*  1 软件　
*  2 话题　
*  3 博客　
*  4 新闻　
*  5 代码
*
*/
// router.get('/favorite_list/:uid/:type',function(req,res,next){
//   var url=[OSCAPI.favorite_list,'uid='+req.params.uid+'&type='+req.params.type].join('?');
//   var result={};
//
//   httpHelper.get(url,30000,function(err,xml){
//     if(err){
//       console.log(err);
//       next();
//     }
//     parseString(xml, {explicitArray:false,ignoreAttrs:true},function (err, data) {
//         //console.log(JSON.stringify(data));
//         result.data=data;
//         res.json(result);
//     });
//   })
// })

router.post('/favorite_list',function(req,res,next){
    var url=[OSCAPI.favorite_list,'uid='+req.body.uid+'&type='+req.body.type].join('?');
    var Cookie=['_reg_key_=7ff;',req.body.key].join(' ');
    var result={};
    console.log('Cookie:'+Cookie);
    httpHelper.get(url,30000,function(err,xml){
      if(err){
          console.log(err);
      }
      //console.log(data);
      parseString(xml, {explicitArray:false,ignoreAttrs:true},function (err, data) {
          console.log(JSON.stringify(data));
          result=data;
          res.json(result);
      });
    },'utf-8',{
      'Cookie':Cookie
    })
})

module.exports = router;
