var express = require('express');
var httpHelper = require('../utils/httpHelper')
var xml2js = require('xml2js');
var parseString = xml2js.parseString;

var OSCAPI = require('../utils/OSCAPI')

var router = express.Router();

router.get('/list', function(req, res, next) {
  //res.send('respond with a resource');
  var pageIndex=req.query.pageIndex;
  var pageSize=req.query.pageSize;
  var url=[OSCAPI.blog_list,'type='+req.query.type+'&pageIndex='+pageIndex+'&pageSize='+pageSize].join('?');
  console.log('url='+url);
  var result={};
  httpHelper.get(url,30000,function(err,xml){
    if(err){
        console.log(err);
    }
    //console.log(data);
    parseString(xml, {explicitArray:false,ignoreAttrs:true},function (err, data) {
        //console.log(JSON.stringify(data));
        result=data;
        res.json(result);
    });

  })

});

router.get('/blog_detail/:id',function(req,res,next){
  var url=[OSCAPI.blog_detail,'id='+req.params.id].join('?');
  var result={};
  httpHelper.get(url,30000,function(err,xml){
    if(err){
        console.log(err);
        next()
    }
    //console.log(data);
    parseString(xml, {explicitArray:false,ignoreAttrs:true},function (err, data) {
        //console.log(JSON.stringify(data));
        result=data;
        res.json(result);
    });

  })
})

module.exports = router;
